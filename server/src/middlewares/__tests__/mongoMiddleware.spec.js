const mongoMiddleware = require('../mongoMiddleware.js');
const { connection } = require('../../models');

const getRequestMock = () => ({
	app: {},
});


describe('mongoMiddleware test', () => {

	afterAll(() => {
		connection.close();
	});

	it('should set models in request object, and call nextFn function', async () => {
		const nextFn = jest.fn();
		const req = getRequestMock();

		await mongoMiddleware(req, {}, nextFn);

		expect(nextFn).toBeCalled();
		expect(req.app.models).toBeInstanceOf(Object);
	});
});

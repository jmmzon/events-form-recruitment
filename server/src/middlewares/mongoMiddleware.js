const mongoModels = require('../models');

module.exports = function mongoMiddleware(req, res, next) {
	req.app.models = mongoModels;
	next();
};

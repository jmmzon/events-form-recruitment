const { Joi } = require('celebrate');

const createEvent = {
	body: {
		firstName: Joi.string().required(),
		lastName: Joi.string().required(),
		email: Joi.string().email().required(),
		date: Joi.date().required()
	}
};


module.exports = {
	createEvent
};

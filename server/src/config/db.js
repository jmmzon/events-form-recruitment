require('dotenv').config();

const dbConfig = {
	development: {
		mongodbUri: 'mongodb://localhost:28017/eventsForm'
	},
	test: {
		mongodbUri: 'mongodb://localhost:28017/eventsFormTest'
	},
	production: {
		mongodbUri: process.env.MONGODB_URI
	}
};

module.exports = dbConfig[process.env.NODE_ENV || 'development'];

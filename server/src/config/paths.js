const path = require('path');

module.exports = {
	STATIC: path.resolve(__dirname, '../static/')
};

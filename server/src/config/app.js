require('dotenv').config();

const appConfig = {
	development: {
		APP_PORT: 8080
	},
	production: {
		APP_PORT: process.env.APP_PORT
	}
};

module.exports = appConfig[process.env.NODE_ENV || 'development'];

const app = require('./app');
const { APP_PORT } = require('./config/app');

app.listen(APP_PORT);

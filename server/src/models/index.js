const mongoose = require('mongoose');
const dbConfig = require('../config/db');

const EventSchema = require('./Event');

const connection = mongoose.createConnection(dbConfig.mongodbUri, {useNewUrlParser: true, useUnifiedTopology: true});

module.exports = {
	Event: connection.model('Event', EventSchema)
};

module.exports.connection = connection;

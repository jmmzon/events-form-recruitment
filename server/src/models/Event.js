const { Schema } = require('mongoose');
const validator = require('validator');

module.exports = new Schema({
	firstName: {
		type: String,
		required: true
	},
	lastName: {
		type: String,
		required: true
	},
	email: {
		type: String,
		required: true,
		validator: validator.isEmail
	},
	date: {
		type: Date,
		required: true
	}
});

const app = require('../../app');
const request = require('supertest');
const {connection} = require('../../models');

describe('events routes tests', () => {
	afterAll(() => {
		connection.close();
	});

	it('should save & return new event in db, if passed data are correct', async () => {
		const response = await request(app)
			.post('/events')
			.send({
				firstName: 'Jan',
				lastName: 'Zon',
				email: 'jmmzon@gmail.com',
				date: '2019-01-01'
			});


		expect(response.status).toBe(201);
		expect(response.body).toMatchObject({
			firstName: 'Jan',
			lastName: 'Zon',
			email: 'jmmzon@gmail.com',
			date: '2019-01-01T00:00:00.000Z',
			_id: expect.any(String)
		});
	});

	it('should return code 400, and valdation result if passed data are incorrect', async () => {
		const response = await request(app)
			.post('/events')
			.send({
				firstName: '1212',
				lastName: '1212',
				email: null,
				date: 'string'
			});

		expect(response.status).toBe(400);
		expect(response.body).toMatchObject({
			statusCode: 400,
			error: 'Bad Request',
			message: expect.any(String),
			validation: expect.any(Object),
		});

	});


});

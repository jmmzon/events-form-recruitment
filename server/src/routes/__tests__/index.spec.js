const app = require('../../app');
const request = require('supertest');
const { connection } = require('../../models')

describe('general routes tests', function () {
	afterAll(() => {
		connection.close();
	});

	it('should return docs.html on / route', async () => {
		const response = await request(app).get('/');

		expect(response.status).toBe(200);
		expect(response.text).toMatchSnapshot();
	});

	it('should return 404 error, if route is not defined', async () => {
		const response = await request(app).get('/not/existing/route');

		expect(response.status).toBe(404);
		expect(response.body).toMatchObject({
			errorMessage: 'Not found'
		});
	});

});

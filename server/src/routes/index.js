const path = require('path');
const paths = require('../config/paths');
const router = require('express').Router();
const eventsRoutes = require('./events');

router.use('/events', eventsRoutes);


router.get('/', (req, res) => {
	return res.sendFile(path.join(paths.STATIC, 'docs.html'))
});

router.use('*', (req, res) => res.status(404).send({errorMessage: 'Not found'}));

module.exports = router;

const router = require('express').Router();
const {celebrate} = require('celebrate');


const eventsSchemas = require('../schemas/events');


const postCreateEvent = async (req, res) => {
	const { body: eventData, app: { models } } = req;
	const { Event } = req.app.models;

	try {
		const event = new Event(eventData);
		const savedEvent = await event.save();

		res.status(201).send(savedEvent.toObject({ versionKey: false }));
	} catch (e) {
		console.error(e);
		res.send(e.message).status(500);
	}
};


router.post('/', celebrate(eventsSchemas.createEvent), postCreateEvent);


module.exports = router;

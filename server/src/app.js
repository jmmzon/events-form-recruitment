require('iconv-lite/encodings');
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const routes = require('./routes');
const {errors: celebrateErrorsMiddleware} = require('celebrate');
const mongoDbMiddleware = require('./middlewares/mongoMiddleware');

const app = express();

app.use(cors());
app.use(express.json());
app.use(mongoDbMiddleware);

app.use(routes);

app.use(celebrateErrorsMiddleware());

module.exports = app;

## Install project


Clone repository
```bash
    git clone repo-url
```

Install backend
```bash
    cd contract-form/server
    yarn install
```

Install frontend
```bash
   cd ../contract-form/client
   yarn install
```

## Run for development

#### Start backend

```bash
   cd {cloned-repository-path}/server
   yarn docker:start
   yarn server:start:dev
```


#### Start frontend

```bash
   cd {cloned-repository-path}/client
   cp .env.example .env #copy env file
   yarn server:start:dev
```

## Run for production

#### Frontend
```bash
   cd {cloned-repository-path}/client
   cp .env.example .env #copy env file
   #update .env REACT_API_BASE_URL to server url
   yarn build
   # deploy files to hosting 
```

#### Backend
```bash
    cd {cloned-repository-path}/server
    cp .env.example .env
    #fill required config options in .env file using favourite editor
    # set NODE_ENV to production and run start-server.js
    # WARN: use process manager, to restart app on possible crash
    #       For example: NODE_ENV=production pm2 start src/start-server.js
```
###### TODO: Serve client files using express

## Run tests

#### Backend
```bash
   cd {cloned-repository-path}/server
   yarn docker:start
   yarn test
```


#### Frontend

```bash
   cd {cloned-repository-path}/client
   yarn test
```

## Possible improvements

#### Frontend

* Use one of well known validating library, or
* in my solution:
    *  <strike>add touched form input state, not display errors at start~~</strike
    
    
#### Backend
* add linting
* add building for production
* add support for serving static files
* add server side rendering

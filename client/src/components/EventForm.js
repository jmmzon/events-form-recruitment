import React, {
    forwardRef,
    useImperativeHandle
} from 'react';
import { useForm } from '../lib/form-validation';
import { head } from 'ramda';
import {
    isEmail,
    isRequired
} from '../lib/form-validation/validators';
import {
    Button,
    Card,
    Col,
    Form,
    Row
} from 'react-bootstrap';
import * as PropTypes from 'prop-types';

const fields = [
    {
        type: 'text',
        name: 'firstName',
        label: 'First name',
        validators: [isRequired]
    },
    {
        type: 'text',
        name: 'lastName',
        label: 'Last name',
        validators: [isRequired]
    },
    {
        type: 'email',
        name: 'email',
        label: 'Email',
        validators: [isRequired, isEmail]
    },
    {
        type: 'date',
        name: 'date',
        label: 'Date',
        validators: [isRequired]
    }
];


function EventForm({ onSubmit, innerRef }) {
    const {
        data, errors, touched, isValid,
        updateFormValue, resetForm
    } = useForm('eventForm', fields);

    useImperativeHandle(innerRef, () => ({
        resetForm
    }));

    return (
        <Card className={ 'mt-5' }>
            <Card.Body>
                <Card.Title>Sign up for event</Card.Title>
                <Form className={ 'EventForm' } onSubmit={ (event) => {
                    onSubmit(data);
                    event.preventDefault();
                } }>
                    {
                        fields.map(({ type, name, label }) => (
                            <Form.Group
                                as={ Row }
                                controlId={ name }
                                className={ { 'has-danger': head(errors[name]) } }
                                key={ name }
                            >
                                <Form.Label column={ true } sm={ 4 }>{ label }</Form.Label>
                                <Col>
                                    <Form.Control
                                        sm={ 8 }
                                        onChange={ updateFormValue }
                                        onBlur={ updateFormValue }
                                        name={ name }
                                        isInvalid={ touched[name] && head(errors[name]) }
                                        value={ data[name] }
                                        type={ type }
                                    >
                                    </Form.Control>
                                    {
                                        touched[name] && head(errors[name]) && (
                                            <Form.Text className='text-danger'>
                                                { head(errors[name]) }
                                            </Form.Text>
                                        )
                                    }
                                </Col>
                            </Form.Group>
                        ))
                    }
                    <Button
                        className="float-right"
                        type={ 'submit' }
                        disabled={ !isValid }
                    >
                        Sign up!
                    </Button>
                </Form>
            </Card.Body>
        </Card>

    );
}

EventForm.propTypes = {
    onSubmit: PropTypes.func.isRequired,
    innerRef: PropTypes.object
};

export default forwardRef((props, ref) => (
    <EventForm innerRef={ ref } { ...props }/>
));

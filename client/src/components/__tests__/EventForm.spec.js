import React from 'react';
import renderer from 'react-test-renderer';
import EventForm from '../EventForm';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import { createInitialState } from '../../lib/form-validation/createFormReducer';
import { noop } from "../../lib/fp";


const mockStore = configureMockStore();
describe('components/EventForm.js', () => {
    let store, eventForm;

    beforeEach(() => {
        store = mockStore({
            eventForm: createInitialState({ firstName: '', lastName: '', date: '', email: '' })
        });

        eventForm = renderer.create(
            <Provider store={ store }>
                <EventForm onSubmit={ noop }/>
            </Provider>);
    });

    it('event form component should match snapshot', () => {
        expect(eventForm).toMatchSnapshot();
    });
});

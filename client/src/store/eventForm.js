import { createFormReducer } from '../lib/form-validation/createFormReducer';

export default createFormReducer(
	'eventForm',
	{
		firstName: '',
		lastName: '',
		email: '',
		date: ''
	}
);

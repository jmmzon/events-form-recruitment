import { combineReducers, createStore } from 'redux';
import eventForm from './eventForm';



export const store = createStore(
	combineReducers({
		eventForm: eventForm

	}),
	window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);



import React, { useRef } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.css';
import 'react-toastify/dist/ReactToastify.css';

import { Provider } from 'react-redux';
import { store } from './store';
import EventForm from './components/EventForm';
import { Col, Row } from 'react-bootstrap';
import { toast, ToastContainer } from 'react-toastify';

import api from './lib/api';

function App() {
	const form = useRef();

	const submit = async (formData) => {
		try {
			await api.events.create(formData);
			form.current.resetForm();
			toast.success('Event saved');
		} catch (e) {
			toast.error('Error has occured. Details in console');
			console.error(e);
		}
	};

	return (
		<Provider store={store}>
			<Row>
				<Col
					xs={12}
					xlg={{ span: 4, offset: 4 }}
					lg={{ span: 6, offset: 3 }}
					md={{ span: 8, offset: 2 }}
				>
					<EventForm onSubmit={submit} ref={form}/>
				</Col>
				<ToastContainer/>
			</Row>
		</Provider>
	);
}

export default App;

import {
	createValidator,
	isEmail
} from '../validators';

describe('form-validation/validators tests', () => {
	it('should create custom validator', () => {
		const validator = createValidator(((arg) => arg === 'This is validator'), 'please enter: "This is validator" phrase');
		expect(validator).toBeInstanceOf(Function);
		expect(validator('This is validator')).toBe('');
		expect(validator('This is not validator')).toBe('please enter: "This is validator" phrase');
	});

	it('isEmail validator should return empty string on valid email', () => {
		expect(isEmail('email@example.com')).toBe('');
	});

	it('isEmail validator should return "This is not valid email address" on invalid email', () => {
		expect(isEmail('thisisnotanemail')).toBe('This is not valid email address');
	});
});

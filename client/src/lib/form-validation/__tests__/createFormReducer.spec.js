import {
	createFormReducer,
	createInitialState
} from '../createFormReducer';
import { clone } from 'ramda';
import {
	createSetErrorsAction,
	createSetFieldAction
} from '../actions';

describe('form-validation/createFormReducer/createInitialState tests', () => {
	it('should create initial state for formReducer', () => {
		const initialState = createInitialState({
			firstName: '',
			lastName: ''
		});

		expect(initialState).toEqual({
			data: {
				firstName: '',
				lastName: '',
			},
			errors: {
				firstName: [],
				lastName: [],
			},
			touched: {
				firstName: false,
				lastName: false,
			}
		});
	});
});

describe('form-validation/createFormReducer/reducer tests', () => {
	let reducer;
	const initialValue = {
		firstName: ''
	};
	let initialState;

	beforeEach(() => {
		reducer = createFormReducer('FORM', clone(initialValue));
		initialState = createInitialState(initialValue);
	});

	it('reducer should be a function', () => {
		expect(reducer).toBeInstanceOf(Function);
	});

	it('reducer should return untouched state, when invalid action is passed in', () => {
		expect(reducer(initialState, { type: 'INVALID_ACTION_NAME' })).toEqual({
			data: { firstName: '' },
			errors: { firstName: [] },
			touched: {firstName: false}
		});
	});

	it('reducer should update state.data, when setFieldAction is passed in', () => {
		const action = createSetFieldAction('FORM', 'firstName', 'Jan');
		expect(reducer(initialState, action)).toEqual({
			data: { firstName: 'Jan' },
			errors: { firstName: [] },
			touched: {firstName: false}
		});
	});

	it('reducer should update state.errors, when setErrorsAction is passed in', () => {
		const action = createSetErrorsAction('FORM', 'firstName', ['This file is required']);
		expect(reducer(initialState, action)).toEqual({
			data: { firstName: '' },
			errors: { firstName: ['This file is required'] },
			touched: {firstName: false}
		});
	});
});

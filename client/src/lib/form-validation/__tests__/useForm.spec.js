import React from 'react';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import {
	configure,
	mount
} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { useForm } from '../index';
import { isRequired } from '../validators';
import {
	createInitialState
} from '../createFormReducer';
import {
	SET_ERRORS_ACTION_TYPE, SET_FIELD_TOUCHED_ACTION_TYPE,
	SET_FIELD_VALUE_ACTION_TYPE
} from '../actions';

configure({ adapter: new Adapter() });
const mockStore = configureMockStore();

const fields = [{
	name: 'firstName',
	validators: [isRequired]
}];

function HookWrapper(props) {
	const hook = props.hook ? props.hook() : undefined;
	return <div hook={ hook }/>;
}

describe('form-validation/useForm hook tests', () => {
	let store, wrapper, hook;

	beforeEach(() => {
		store = mockStore({
			FORM: createInitialState({ firstName: 'Jan' })
		});

		wrapper = mount(<Provider store={ store }>
			<HookWrapper hook={ () => useForm('FORM', fields) }/>
		</Provider>);

		({ hook } = wrapper.find('div').props());
	});


	it('should return collection of methods and data to manipulate form', () => {
		expect(hook.resetForm).toBeInstanceOf(Function);
		expect(hook.updateFormValue).toBeInstanceOf(Function);
		expect(store.getActions().length).toBe(1);
		expect(hook.data).toEqual({
			firstName: 'Jan'
		});
	});

	it('should dispatch store actions: setErrors and setField', () => {
		expect(store.getActions().length).toBe(1);
		hook.updateFormValue({target: {name: 'firstName', value: ''}});
		const actions = store.getActions();
		expect(actions.length).toBe(4);
		const [, setField, setTouched, setErrors] = store.getActions();

		expect(setField).toEqual({
			type: SET_FIELD_VALUE_ACTION_TYPE('FORM'),
			payload: {name: 'firstName', value: ''}
		});

		expect(setTouched).toEqual({
			type: SET_FIELD_TOUCHED_ACTION_TYPE('FORM'),
			payload: {name: 'firstName'}
		});

		expect(setErrors).toEqual({
			type: SET_ERRORS_ACTION_TYPE('FORM'),
			payload: {name: 'firstName', errors: ['This field is required']}
		});
	});
});

import {
	createSetFieldAction,
	RESET_FORM_ACTION_TYPE,
	SET_ERRORS_ACTION_TYPE,
	SET_FIELD_VALUE_ACTION_TYPE
} from '../actions';

describe('form-validation/actions tests', () => {
	it('should create appropriate actions types names', () => {
		expect(SET_FIELD_VALUE_ACTION_TYPE('FORM')).toBe('form/FORM/setFieldValue');
		expect(SET_ERRORS_ACTION_TYPE('FORM')).toBe('form/FORM/setErrors');
		expect(RESET_FORM_ACTION_TYPE('FORM')).toBe('form/FORM/resetForm');
	});

	//NOTE: for other actions tests remain similar
	it('should create setField action with appropriate payload', () => {
		expect(
			createSetFieldAction('FORM', 'firstName', 'Jan')
		).toEqual({
			type: 'form/FORM/setFieldValue',
			payload: {
				name: 'firstName',
				value: 'Jan'
			}
		});
	});
});


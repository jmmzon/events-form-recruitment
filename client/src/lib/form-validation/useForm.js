import { useEffect } from 'react';

import {
	useDispatch,
	useSelector
} from 'react-redux';

import {
	identity,
	isEmpty,
	path,
	propEq
} from 'ramda';

import { applyToMultiple } from '../fp';

import {
	createResetFormAction,
	createSetErrorsAction,
	createSetFieldAction,
	createSetFieldTouched
} from './actions';


const validate = (validators, data, name, value) => {
	return validators
		.map(applyToMultiple(value, name, data))
		.filter(identity);
};

export default function useForm(formName, fields) {
	const { data, errors, touched } = useSelector(path([formName]));
	const dispatch = useDispatch();

	const updateFormValue = ({ target: { name, value } }) => {
		const { validators = [] } = fields.find(propEq('name', name));

		dispatch(createSetFieldAction(formName, name, value));
		dispatch(createSetFieldTouched(formName, name));
		dispatch(createSetErrorsAction(
			formName, name,
			validate(validators, data, name, value)
		));
	};

	const validateForm = () => fields.forEach(({ name, validators }) => {
		dispatch(createSetErrorsAction(
			formName, name,
			validate(validators, data, name, data[name]))
		);
	});

	const resetForm = () => {
		dispatch(createResetFormAction(formName));
		validateForm();
	};

	useEffect(
		() => {
			validateForm();
		}, 	// eslint-disable-next-line react-hooks/exhaustive-deps
		[]
	);

	return {
		data,
		errors,
		touched,
		//should be keep in state
		isValid: isEmpty(Object.values(errors).flat()),
		resetForm,
		updateFormValue
	};
}

import {identity} from 'ramda';
import IsEmail from 'isemail';

export const createValidator = (predicate, errorMessage) =>
	(...args) =>
		!predicate(...args) ? errorMessage: '';


export const isRequired = createValidator(identity, 'This field is required');
export const isEmail = createValidator(IsEmail.validate, 'This is not valid email address');

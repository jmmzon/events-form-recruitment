import { always, assocPath, clone, equals, map } from 'ramda';
import {
	RESET_FORM_ACTION_TYPE,
	SET_ERRORS_ACTION_TYPE,
	SET_FIELD_TOUCHED_ACTION_TYPE,
	SET_FIELD_VALUE_ACTION_TYPE
} from './actions';


export const createInitialState = (initialValue) => ({
	data: clone(initialValue),
	errors: map(() => [], initialValue),
	touched: map(always(false), initialValue)
});


export const createFormReducer = (formName, initialValue) => {
	return function formReducer(state = createInitialState(initialValue), { type, payload }) {
		const isTypeOf = equals(type);

		if (isTypeOf(SET_FIELD_VALUE_ACTION_TYPE(formName))) {
			return assocPath([ 'data', payload.name ], payload.value, state);
		}

		if (isTypeOf(SET_ERRORS_ACTION_TYPE(formName))) {
			return assocPath([ 'errors', payload.name ], payload.errors, state);
		}

		if (isTypeOf(RESET_FORM_ACTION_TYPE(formName))) {
			return createInitialState(initialValue);
		}

		if (isTypeOf(SET_FIELD_TOUCHED_ACTION_TYPE(formName))) {
			return assocPath([ 'touched', payload.name ], true, state);
		}

		return state;
	};
};

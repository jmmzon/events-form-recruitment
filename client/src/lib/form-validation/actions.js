export const SET_FIELD_VALUE_ACTION_TYPE = (formName) => `form/${formName}/setFieldValue`;
export const SET_ERRORS_ACTION_TYPE = (formName) => `form/${formName}/setErrors`;
export const RESET_FORM_ACTION_TYPE = (formName) => `form/${formName}/resetForm`;
export const SET_FIELD_TOUCHED_ACTION_TYPE = (formName) => `form/${formName}/setFieldTouched`;

export const createSetFieldAction = (formName, name, value) => ({
	type: SET_FIELD_VALUE_ACTION_TYPE(formName),
	payload: { name, value },
});

export const createSetErrorsAction = (formName, name, errors) => ({
	type: SET_ERRORS_ACTION_TYPE(formName),
	payload: {name, errors},
});

export const createSetFieldTouched = (formName, name) => ({
	type: SET_FIELD_TOUCHED_ACTION_TYPE(formName),
	payload: {name}
});


export const createResetFormAction = (formName) => ({
	type: RESET_FORM_ACTION_TYPE(formName),
});

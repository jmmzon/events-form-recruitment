import { prop } from 'ramda';

const apiBaseUrl = process.env.REACT_APP_API_BASE_URL;

class RequestError extends Error {
	message = 'Request failed';

	constructor(code, data) {
		super();
		this.data = data;
		this.code = code;
	}
}

const responseHandler = async (response) => {
	if(response.status >= 400) {
		throw new RequestError(response.status, await response.json());
	} else {
		return {
			...response,
			data: await response.json()
		};
	}
};

export const apiPost = (url, data, options) => {
	return fetch(`${apiBaseUrl}/${url}`, {
		method: 'POST',
		body: JSON.stringify(data),
		headers: {
			'Content-Type': 'application/json'
		},
		...options,
	}).then(responseHandler);
};


export default {
	events: {
		create(eventData) {
			return apiPost('events', eventData).then(prop('data'));
		}
	}
};

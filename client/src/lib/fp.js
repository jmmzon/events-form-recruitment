export const applyToMultiple = (...args) =>
	(fn) => fn(...args);

export const noop = () => {};

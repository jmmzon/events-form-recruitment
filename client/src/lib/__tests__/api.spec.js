import api, { apiPost } from '../api';
import fetchMock from 'fetch-mock';

describe('apiPost wrapper test', () => {
	afterEach(() => {
		fetchMock.reset();
	});

	it('test apiPost success request', async () => {
		fetchMock.once('*', { firstName: 'Jan' });
		const { data } = await apiPost('events', { firstName: 'Jan' });
		expect(data).toEqual({ firstName: 'Jan' });

		expect(fetchMock.lastCall()[0]).toBe('http://localhost:8080/events');
		expect(fetchMock.lastCall()[1].method).toBe('POST');
	});
});

describe('test api.events.create', () => {
	it('should make correct request: POST /events', async () => {
		fetchMock.once('*', { firstName: 'Jan', lastName: 'Piotr', '_id': 12 });
		const event = await api.events.create({firstName: 'Jan', lastName: 'Piotr'});
		expect(event).toEqual({
			firstName: 'Jan',
			lastName: 'Piotr',
			'_id': 12
		});
		const [url, options] = fetchMock.lastCall();
		expect(url).toBe('http://localhost:8080/events');
		expect(options.method).toBe('POST');
		expect(options.body).toBe(JSON.stringify({ firstName: 'Jan', lastName: 'Piotr' }));
	});

});
